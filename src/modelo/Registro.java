package modelo;

import java.util.ArrayList;

public class Registro {
	
	private ArrayList<Localidad> localidades;
	
	
	public Registro()
	{
		this.localidades = new ArrayList<Localidad>();
	}
	
	public void agregarLocalidad(String nombre, String provincia, int latitud, int longitud) 
	{
		Localidad localidad_nueva = new Localidad(nombre, provincia, latitud,longitud);
		
		if(!this.localidades.contains(localidad_nueva)) {
			this.localidades.add(localidad_nueva);
		}
		else {
			throw new RuntimeException("Datos repetidos: " + nombre + ", " + latitud + ", " + longitud);
		}
	}
	
	private int distancias_entre_localidades(Localidad inicial, Localidad fin) 
	{
		int latitud1 = inicial.getLatitud();
		int longitud1 = inicial.getLongitud();
		int latitud2 = fin.getLatitud();
		int longitud2 = fin.getLongitud();
		
		int x = Math.max(latitud1, latitud2) - Math.min(latitud1, latitud2);
		int y = Math.max(longitud1, longitud2) - Math.min(longitud1,longitud2);
		
		int d = (int) Math.sqrt(x*x+y*y);
		
		return d;
		
	}
	
	public int[][] CalcularCostos() 
	{
		
		int[][] costos = new int[this.localidades.size()][this.localidades.size()];
		for(int i = 0; i<this.localidades.size(); i++) {
		for(int j = 0; j<this.localidades.size(); j++) {
			
			if(i == j) {costos[i][j] = 0;}
			else {
				int distancia = distancias_entre_localidades(this.localidades.get(i), this.localidades.get(j));
				costos[i][j] = distancia;
				if(distancia>300) {
					costos[i][j] = (int) (distancia*2);
				}
				if(!localidades.get(i).getProvincia().equals(localidades.get(j).getProvincia())) 
				{
					costos[i][j]+=100;
				}
			}
		}
		}
		return costos;
	}
	
}
