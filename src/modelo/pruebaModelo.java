package modelo;

public class pruebaModelo {

	public static void main(String[] args) {
		// TODO Apéndice de método generado automáticamente
		
		Registro reg = new Registro();
		reg.agregarLocalidad("gb", "buenos aires", 20 , 10);
		reg.agregarLocalidad("jose c paz", "buenos aires", 30, 15);
		reg.agregarLocalidad("rawson", "chubut", 60, 44);
		reg.agregarLocalidad("salta", "salta", 10, 10);
		
		int[][] mat = reg.CalcularCostos();
		
		for(int f = 0; f<mat[0].length; f++) {
			for(int c = 0; c<mat[f].length; c++) {
				System.out.print(mat[f][c]+" ");
			}
			System.out.println();
		}
		
		System.out.println();
		
		int[][] mat1 = Solver.Algoritmo_Prim(reg.CalcularCostos());
		
		for(int f = 0; f<mat1[0].length; f++) {
			for(int c = 0; c<mat1[f].length; c++) {
				System.out.print(mat1[f][c]+" ");
			}
			System.out.println();
		}
	}

}
