package modelo;

import java.util.ArrayList;

//El ejecicio viene comentado explicando las instrucciones y metodos utilizados para realizar el algoritmo de prim
public class Solver 
{
  public static int [][] Algoritmo_Prim(int [][] MatrizOriginal)
  {
      //Primeramente declaramos 3 Listas 
      ArrayList<Boolean> verticesVisitados = new ArrayList<Boolean>();
      ArrayList<Integer> distanciasRelativas = new ArrayList<Integer>();
      ArrayList<Integer> nodosAdyacentes = new ArrayList<Integer>();
      /**
      Inicializando Variables
      */
      for(Integer contador=0;contador < MatrizOriginal[0].length;contador++)
      {
          verticesVisitados.add(false);
          nodosAdyacentes.add(0);
          distanciasRelativas.add(Integer.MAX_VALUE);
      }
      distanciasRelativas.set(0, 0);
      /**
      Definicion del punto que sera la raiz del punto resultante
      */
      Integer puntoevaluado = 0;
      Integer Adyacentes = 0;
      /**
      Estructuras para ejecutar algoritmo de Prim
      */
      for(Integer contadorpuntoevaluado = 0;contadorpuntoevaluado < MatrizOriginal[0].length;contadorpuntoevaluado++)
      {
          for(Integer contadorAdyacentes = 0;contadorAdyacentes < MatrizOriginal[0].length;contadorAdyacentes++)
          {
              /**
              Verifica si el nodo a ser valorado en esta iteracion ha sido valorado anteriormente ;
              * si es asi, pasa para la siguiente iteracion
              */
              if((verticesVisitados.get(contadorAdyacentes)) || (contadorAdyacentes == puntoevaluado))
              {
                  continue;
              }
              /**
              Dos comparaciones:
              * 1-Verifica si la matrizOriginal hay algo de valor en la columna que es > 0.Si es asi
              * Significa que existe un Arista entre estos dos puntos de la gr�fica 
              * 2-Verifica si el peso de la arista entre los dos puntos es menor que el actual distanciaRelativa
              * del nodo vecino
              * Si es correcto, el nodo distanciaRelativa para ser evaluado por el momento sera actualizado
              * el valor de esta nueva distancia valorada para puntoValorado
              */
              if((MatrizOriginal[puntoevaluado][contadorAdyacentes] > 0) && ((MatrizOriginal[puntoevaluado][contadorAdyacentes] < distanciasRelativas.get(contadorAdyacentes))))
              {
                  distanciasRelativas.set(contadorAdyacentes, MatrizOriginal[puntoevaluado][contadorAdyacentes]);
                  nodosAdyacentes.set(contadorAdyacentes,puntoevaluado);
              }
              /**
              _Marca los vertices d puntoevaluado como un vertice ya verificado 
              */
              verticesVisitados.set(puntoevaluado,true);//*Paso de parametro true y de ahicomienza siguiente Vertice */
              /**
              *Prepara para seleccionar el proximo v�rtice a ser evaluado 
              */
              puntoevaluado = 0;
              Integer distanciaActualacomparar = Integer.MAX_VALUE;
              
              /**
              *Seleccionar el pr�ximo vertice a ser evaluado
              */
              for(Integer contador =1;contador < verticesVisitados.size();contador++)
              {
                  /**
              *Si el vertice a ser verificado ya fue evaluado anteriormente (true)
              * pasa a la proxima iteraci�n
              */
                  if(verticesVisitados.get(contador))
                  {
                      continue;

                  }
              /**
              *Si la distancia relativa de ese punto es menor que a al punto valorado
              * asumir ese nuevo punto como un punto valorado
              * 
              * al final de la iteracion, ser� seleccionado un punto con menor distancia relativa
              */
                  if(distanciasRelativas.get(contador) < distanciaActualacomparar)
                  {
                      distanciaActualacomparar = distanciasRelativas.get(contador);
                      puntoevaluado = contador;
                  }
              
              }//end 3er for 
      }//end 2do for
      }//end 1er for
      int[][] matrizResultante =new int [MatrizOriginal[0].length][MatrizOriginal[0].length];
              /**
              *Crear una MatrizOriginal como una arbol resultante del Algoritmo de Prim 
              */
              for(int contador = 0;contador < nodosAdyacentes.size();contador++)
              {
                  matrizResultante[contador][nodosAdyacentes.get(contador)] = MatrizOriginal[contador][nodosAdyacentes.get(contador)];
                  matrizResultante[nodosAdyacentes.get(contador)][contador] = matrizResultante[contador][nodosAdyacentes.get(contador)];
              }
      return matrizResultante;
  }//end funcion Prim

}
