package modelo;

public class Localidad {
	
	private String nombre;
	private String provincia;
	private int latitud;
	private int longitud;
	
	

	public Localidad(String nombre, String provincia, int latitud, int longitud) {
		this.nombre = nombre;
		this.provincia = provincia;
		this.latitud = latitud;
		this.longitud = longitud;
	}

	public int getLatitud() {
		return this.latitud;
	}

	public int getLongitud() {
		return this.longitud;
	}
	
	public String getProvincia() {
		return this.provincia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + latitud;
		result = prime * result + longitud;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Localidad other = (Localidad) obj;
		if (latitud != other.latitud)
			return false;
		if (longitud != other.longitud)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	
	
}
